
import AMapLoader from '@amap/amap-jsapi-loader'
export default function loadAMap (option) {
  const params = Object.assign({
    key: '42efdd21d2128df1ec52fce1fe12a2c2', // 申请好的Web端开发者Key，首次调用 load 时必填
    version: '2.0' // 指定要加载的 JSAPI 的版本，缺省时默认为 1.4.15
    // plugins: [], // 需要使用的的插件列表，如比例尺'AMap.Scale'等
  }, option)
  return AMapLoader.load(params)
    .then(_ => _)
    .catch((e) => {
      console.log(e)
    })
}
