import Vue from 'vue'
import VueI18n from 'vue-i18n'

import en from './locale/en'
import zh from './locale/zh'

Vue.use(VueI18n)

const i18n = new VueI18n({
  locale: 'zh',
  messages: {
    en,
    zh
  }
})

export default i18n
