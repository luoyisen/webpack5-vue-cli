
import zhLocale from 'element-ui/lib/locale/lang/zh-CN'
export default {
  systemTitle: '后台管理系统',
  menu: {
    map: '地图',
    amap: '高德地图',
    dashboard: '首页',
    feature: '功能',
    compressedImage: '图片压缩',
    camera: '照相机'
  },
  setting: {
    routerCardFullScreen: '全屏'
  },
  ...zhLocale
}
