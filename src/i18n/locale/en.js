
import enLocale from 'element-ui/lib/locale/lang/en'
export default {
  systemTitle: 'Background management system',
  menu: {
    map: 'Map',
    amap: 'Amap',
    dashboard: 'Dashboard',
    feature: 'feature',
    compressedImage: 'compressedImage',
    camera: 'camera'
  },
  setting: {
    routerCardFullScreen: 'FullScreen'
  },
  ...enLocale
}
