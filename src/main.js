
import Vue from 'vue'
import router from './router'
import App from './App.vue'

// element
import './util/element.js'
// i18n
import i18n from './i18n/index.js'

new Vue({
  i18n,
  router,
  render: h => h(App)
})
  .$mount('#app');
