import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'

try {
  const originalPush = Router.prototype.push
  Router.prototype.push = function push (location) {
    return originalPush.call(this, location).catch(err => err)
  }
} catch (e) {
  /* eslint-disable */
}

Vue.use(Router);

export default new Router({
    scrollBehavior: () => ({ y: 0 }),
    routes,
});