/**
 *  meta属性
 *  index: element的index属性,
 *  icon: element的icon,
 *  title: i18n的路径,
 *  isShow: 是否展示路由,
 *  keepAlive: 是否使用<keep-alive>缓存路由,
 */

import dashboard from '@/views/dashboard/dashboard.vue'
import RouterView from '@/components/layouts/RouterView.vue'

export default [
  {
    path: '/',
    redirect: '/dashboard'
  },
  {
    path: '/dashboard',
    component: dashboard,
    meta: {
      index: 'dashboard',
      icon: 'el-icon-s-home',
      title: 'menu.dashboard',
      isShow: true,
      keepAlive: false
    }
  },
  {
    path: '/feature',
    component: RouterView,
    meta: {
      index: 'feature',
      icon: 'el-icon-film',
      title: 'menu.feature',
      isShow: true,
      keepAlive: false
    },
    children: [
      {
        path: 'compressedImage',
        component: () => import(/* webpackChunkName: 'compressedImage' */ '@/views/feature/compressedImage.vue'),
        meta: {
          index: 'compressedImage',
          icon: '',
          title: 'menu.compressedImage',
          isShow: true,
          keepAlive: true
        }
      },
      {
        path: 'camera',
        component: () => import(/* webpackChunkName: 'camera' */ '@/views/feature/camera.vue'),
        meta: {
          index: 'camera',
          icon: '',
          title: 'menu.camera',
          isShow: true,
          keepAlive: false
        }
      }
    ]
  },
  {
    path: '/map',
    component: RouterView,
    meta: {
      index: 'map',
      icon: 'el-icon-map-location',
      title: 'menu.map',
      isShow: true,
      keepAlive: true
    },
    children: [
      {
        path: 'amap',
        component: () => import(/* webpackChunkName: 'amap' */ '@/views/map/AMap.vue'),
        meta: {
          index: 'amap',
          icon: '',
          title: 'menu.amap',
          isShow: true,
          keepAlive: true
        }
      }
    ]
  }
]
