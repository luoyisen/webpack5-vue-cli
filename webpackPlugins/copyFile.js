
let fs = require('fs');
module.exports = class CopyFile {
    #option
    constructor(option) {
        this.#option = option
    }
    apply(compiler) {
        compiler.hooks.done.tap('MyPlugin', (params) => {
            let { to, from } = this.#option.done
            fs.copyFileSync(from, to)
        });
    }
}

