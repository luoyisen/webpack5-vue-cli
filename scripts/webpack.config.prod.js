const path = require('path');
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const TerserPlugin = require('terser-webpack-plugin');

// 自己写的CopyFile插件
// const CopyFile = require('../webpackPlugins/CopyFile')

// This Webpack plugin allows you to copy, archive (.zip/.tar/.tar.gz), move, delete files and directories before and after builds
// 文件操作插件
const FileManagerPlugin = require('filemanager-webpack-plugin');

const P = (...args) => path.resolve(__dirname, '../', ...args)
module.exports = {
    mode: 'production',
    devtool: 'source-map',
    plugins: [

        // 自己写的CopyFile插件
        // new CopyFile({
        //     done: { from: P('public', 'readme.txt'), to: P('dist', 'readme.txt') },
        // }),
        new FileManagerPlugin({
            events: {
                onEnd: {
                    copy: [
                        { source: P('public', 'readme.txt'), destination: P('dist', 'readme.txt') },
                    ],
                }
            }
        }),
    ],
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all',
                },
            },
        },
        minimizer: [
            `...`,
            new CssMinimizerPlugin(),
            new TerserPlugin({
                parallel: true,
            }),
        ],
    },
};