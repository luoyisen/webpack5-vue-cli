const path = require('path');
const os = require('os')
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const threadsCount = os.cpus().length;

const P = (...args) => path.resolve(__dirname, '../', ...args)

module.exports = {
    entry: P('./src/main.js'),
    output: {
        clean: true,
        path: P('dist'),
        filename: 'js/[name].[contenthash].js',
        chunkFilename: 'js/[name].[contenthash].js',
    },
    module: {
        rules: [
            {
                test: /\.vue$/i,
                use: ['vue-loader'],
            },
            {
                oneOf: [
                    {
                        test: /\.css$/,
                        use: [
                            MiniCssExtractPlugin.loader,
                            'css-loader',
                            {
                                loader: 'postcss-loader',
                                options: {
                                    postcssOptions: {
                                        plugins: [
                                            [
                                                'postcss-preset-env',
                                                {
                                                    // 其他选项
                                                },
                                            ],
                                        ],
                                    },
                                },
                            },
                        ]
                    },
                    {
                        test: /\.s[ac]ss$/i,
                        use: [
                            MiniCssExtractPlugin.loader,
                            'css-loader',
                            {
                                loader: 'postcss-loader',
                                options: {
                                    postcssOptions: {
                                        plugins: [
                                            [
                                                'postcss-preset-env',
                                                {
                                                    // 其他选项
                                                },
                                            ],
                                        ],
                                    },
                                },
                            },
                            'sass-loader',
                        ],
                    },
                    {
                        test: /\.(png|jpe?g|gif|svg)$/,
                        type: 'asset/resource',
                        generator: {
                            filename: 'image/[contenthash][ext]',
                        },
                    },
                    {
                        test: /\.(ttf|eot|woff2?)$/,
                        type: 'asset/resource',
                        generator: {
                            filename: 'font/[contenthash][ext]',
                        },
                    },
                    {
                        test: /\.js$/,
                        exclude: /node_modules/,
                        use: [
                            {
                                loader: "thread-loader",
                                // 有同样配置的 loader 会共享一个 worker 池
                                options: {
                                    // 产生的 worker 的数量，默认是 (cpu 核心数 - 1)，或者，
                                    // 在 require('os').cpus() 是 undefined 时回退至 1
                                    workers: threadsCount,

                                    // 一个 worker 进程中并行执行工作的数量
                                    // 默认为 20
                                    workerParallelJobs: 50,

                                    // 额外的 node.js 参数
                                    workerNodeArgs: ['--max-old-space-size=1024'],

                                    // 允许重新生成一个僵死的 work 池
                                    // 这个过程会降低整体编译速度
                                    // 并且开发环境应该设置为 false
                                    poolRespawn: false,

                                    // 闲置时定时删除 worker 进程
                                    // 默认为 500（ms）
                                    // 可以设置为无穷大，这样在监视模式(--watch)下可以保持 worker 持续存在
                                    poolTimeout: 2000,

                                    // 池分配给 worker 的工作数量
                                    // 默认为 200
                                    // 降低这个数值会降低总体的效率，但是会提升工作分布更均一
                                    poolParallelJobs: 50,

                                    // 池的名称
                                    // 可以修改名称来创建其余选项都一样的池
                                    name: "my-pool"
                                },
                            },
                            {
                                loader: 'babel-loader',
                                options: {
                                    cacheDirectory: true,
                                    cacheCompression: false,
                                }
                            },
                        ],
                    },
                ]
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'css/[name].css'
        }),
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin({
            template: P('public', 'index.html')
        }),
    ],
    resolve: {
        alias: {
            '@': P('src'),
        },
    },
    performance: {
        hints: false,
    },
};