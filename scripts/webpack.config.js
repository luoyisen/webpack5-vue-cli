const commonConfig = require('./webpack.config.common')
const devConfig = require('./webpack.config.dev')
const prodConfig = require('./webpack.config.prod')
const { merge } = require('webpack-merge');

module.exports = env => {
    if (env.development) return merge(commonConfig, devConfig)
    if (env.production) return merge(commonConfig, prodConfig)
    return merge(commonConfig, prodConfig)
}