const path = require('path');
const os = require('os');
const ESLintPlugin = require('eslint-webpack-plugin');

const threadsCount = os.cpus().length;

const P = (...args) => path.resolve(__dirname, '../', ...args)

module.exports = {
    mode: 'development',
    devtool: 'cheap-module-source-map',
    devServer: {
        hot: true,
        open: true,
        static: './public',
        port: '8088',
        server: 'https',
    },
    plugins: [
        new ESLintPlugin({
            threads: threadsCount,
            cache: true,
            cacheLocation: path.resolve(__dirname, '../node_modules/.cache/.eslintcache'),
            context: path.resolve(__dirname, '../src'),
            exclude: 'node_modules',
            // fix: true,
        }),
    ],
};